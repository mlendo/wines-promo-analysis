//base table to select target group scv_ids
create or replace table LAB_DS.J84536994.wine_target_scv_id as
select HYBRID_SCV_ID as scv_id,tran_dt, tran_id, count(items) as bottles,
case when channel = 1 or channel = 2  then 'Instore'
     when channel = 3 then 'Online' end as Channel

FROM "CUR_LIVE_PCD"."PDL_TRANSFORM_STG_PROD"."W2_LINE_SCV_VX" AS a

where prod_id in (select prod_id
                  from "CUR_LIVE_PCD"."PDL_TRANSFORM_STG_PROD"."D_PROD_WTR" 
                  where PROD_BUY_OFFICE_CD = 'WIN')
                  
and PROMO_FLAG = 1

and HYBRID_SCV_TYPE in ('S','C')
                  
and tran_dt between '2022-11-02' and '2022-11-15'

group by tran_dt, tran_id,Channel,scv_id
having bottles > 5

union 

select b.LATEST_CUST_SCV_ID as scv_id, a.order_dt as tran_dt, a.ORDER_LI_ID as tran_id, sum(QTY) as bottles,
case when SERVICE = 'CELLAR' then 'Cellar' end as Channel

from CONSM_LIVE.CUSTOMER.FBL_WDS_ORDER_LI as a
left join CONSM_LIVE.CUSTOMER.FBL_WDS_ORDER_SCV_VX AS b on a.ORDER_NBR = b.ORDER_NBR 

where SERVICE = 'CELLAR'
                  
and tran_dt between '2022-11-02' and '2022-11-15'

group by tran_dt, tran_id, Channel, scv_id
having bottles > 5;




//table for wine transactions in waitrose 
create or replace table LAB_DS.J84536994.wine_tran as 
select hybrid_scv_id as scv_id, 
           zeroifnull(sum(sales)) as total_wine_spend,
           zeroifnull(sum(case when channel in (1,2) then sales end)) as total_instore_wine_spend,
           zeroifnull(sum(case when channel in (3) then sales end)) as total_online_wine_spend,
           zeroifnull(sum(items)) as total_wine_items,
           zeroifnull(sum(case when channel in (1,2) then sales end)) as total_instore_wine_items,
           zeroifnull(sum(case when channel in (3) then sales end)) as total_online_wine_items
           from "CUR_LIVE_PCD"."PDL_TRANSFORM_STG_PROD"."W2_LINE_SCV_VX"
           where prod_id in (select prod_id
                             from "CUR_LIVE_PCD"."PDL_TRANSFORM_STG_PROD"."D_PROD_WTR" 
                             where PROD_BUY_OFFICE_CD = 'WIN')
           and tran_dt between '2022-08-03' and '2022-11-01'
           group by scv_id;
           
 
 //table for cellar transactions
 create or replace table LAB_DS.J84536994.cellar_tran as      
select LATEST_CUST_SCV_ID as scv_id,
         zeroifnull(sum((PROD_SELLING_PRC*QTY))) as total_cellar_spend,
         zeroifnull(sum(QTY)) as total_sellar_items,
         zeroifnull(count(distinct b.order_id)) as total_cellar_visits
          from CUR_LIVE_PCD.PDL_TRANSFORM_STG_PROD.FBL_WDS_ORDER_LI as a
          left join  CUR_LIVE_PCD.PDL_TRANSFORM_STG_PROD.FBL_WDS_ORDER_SCV_VX as b on a.order_id=b.order_id
          where b.order_dt between '2022-08-03' and '2022-11-01'
          group by scv_id;
          
          
 // table for waitrose transactions
 create or replace table LAB_DS.J84536994.wtr_tran as
select distinct hybrid_scv_id as scv_id, 
zeroifnull(sum (sales)) as total_spend,
zeroifnull(sum(case when channel in (1,2) then sales end)) as total_instore_spend,
zeroifnull(sum(case when channel in (3) then sales end)) as total_online_spend,
zeroifnull(sum(items)) as total_items,
zeroifnull(sum(case when channel in (1,2) then sales end)) as total_instore_items,
zeroifnull(sum(case when channel in (3) then sales end)) as total_online_items,
zeroifnull(count(tran_id)) as total_visits,
zeroifnull(count(case when channel in (1,2) then tran_id end)) as total_instore_visits,
zeroifnull(count(case when channel in (3) then tran_id end)) as total_online_visits
from "CUR_LIVE_PCD"."PDL_TRANSFORM_STG_PROD"."W2_TRAN_SCV_VX"
where tran_dt between '2022-08-02' and '2022-11-01'
group by scv_id
having total_spend > 0;




// table for pre-period customer base
create or replace table LAB_DS.J84536994.pre_period_scv_id as
select distinct hybrid_scv_id as scv_id
from "CUR_LIVE_PCD"."PDL_TRANSFORM_STG_PROD"."W2_TRAN_SCV_VX"
where tran_dt between '2022-08-02' and '2022-11-01'

union

select distinct LATEST_CUST_SCV_ID as scv_id
from CUR_LIVE_PCD.PDL_TRANSFORM_STG_PROD.FBL_WDS_ORDER_SCV_VX
where order_dt between '2022-08-02' and '2022-11-01';

// table for pre-period customer base waitrose only
create or replace table LAB_DS.J84536994.pre_period_scv_id_wtr as
select distinct hybrid_scv_id as scv_id, sum(sales) as sales
from "CUR_LIVE_PCD"."PDL_TRANSFORM_STG_PROD"."W2_TRAN_SCV_VX"

//tables to consolidate features based on scv_id
create or replace table LAB_DS.J84536994.wines_promo_cust_1 as

select scv_id, 
(total_spend) as total_spend,
//Zeroifnull(total_instore_spend) as total_instore_spend,
//Zeroifnull(total_online_spend) total_online_spend,
(total_items) as total_items,
//Zeroifnull(total_instore_items) as total_instore_items ,
//Zeroifnull(total_online_items) as total_online_items,
(total_visits) as total_visits,
//Zeroifnull(total_instore_visits) as total_instore_visits,
//Zeroifnull(total_online_visits) as total_online_visits,
Zeroifnull(total_wine_spend) as total_wine_spend,
//Zeroifnull(total_instore_wine_spend) as total_instore_wine_spend,
//Zeroifnull(total_online_wine_spend) as total_online_wine_spend,
Zeroifnull(total_wine_items) as total_wine_items, 
//Zeroifnull(total_instore_wine_items) as total_instore_wine_items ,
//Zeroifnull(total_online_wine_items) as total_online_wine_items,
//Zeroifnull(total_cellar_spend) as total_cellar_spend,
//Zeroifnull(total_cellar_items) as total_cellar_items,
//Zeroifnull(total_cellar_visits) as total_cellar_visits,
'1' as target

from LAB_DS.J84536994.pre_period_scv_id_wtr A

left join LAB_DS.J84536994.wtr_tran B using (scv_id)

//left join LAB_DS.J84536994.cellar_tran C using (scv_id)
           
left join LAB_DS.J84536994.wine_tran D using (scv_id)

where scv_id in (select scv_id from LAB_DS.J84536994.wine_target_scv_id);
 


//group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19;
//group by 1,2,3,4,5,6;

create or replace table LAB_DS.J84536994.wines_promo_cust_2 as

select scv_id, 
(total_spend) as total_spend,
//Zeroifnull(total_instore_spend) as total_instore_spend,
//Zeroifnull(total_online_spend) total_online_spend,
(total_items) as total_items,
//Zeroifnull(total_instore_items) as total_instore_items ,
//Zeroifnull(total_online_items) as total_online_items,
(total_visits) as total_visits,
//Zeroifnull(total_instore_visits) as total_instore_visits,
//Zeroifnull(total_online_visits) as total_online_visits,
zeroifnull(total_wine_spend) as total_wine_spend,
//Zeroifnull(total_instore_wine_spend) as total_instore_wine_spend,
//Zeroifnull(total_online_wine_spend) as total_online_wine_spend,
zeroifnull(total_wine_items) as total_wine_items, 
//Zeroifnull(total_instore_wine_items) as total_instore_wine_items ,
//Zeroifnull(total_online_wine_items) as total_online_wine_items,
//Zeroifnull(total_cellar_spend) as total_cellar_spend,
//Zeroifnull(total_cellar_items) as total_cellar_items,
//Zeroifnull(total_cellar_visits) as total_cellar_visits,
'0' as target

from LAB_DS.J84536994.pre_period_scv_id_wtr A

left join LAB_DS.J84536994.wtr_tran B using (scv_id)

//left join LAB_DS.J84536994.cellar_tran C using (scv_id)
           
left join LAB_DS.J84536994.wine_tran D using (scv_id)

where scv_id not in (select scv_id from LAB_DS.J84536994.wine_target_scv_id)

//group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19;
//group by 1,2,3,4,5,6;
having total_spend > 0;

where tran_dt between '2022-08-02' and '2022-11-01'

//combines target and control table and selects only a sample for control 
create or replace table LAB_DS.J84536994.wines_promo_cust_wtr as
select *
from LAB_DS.J84536994.wines_promo_cust_1


union

select * 
from LAB_DS.J84536994.wines_promo_cust_2
sample row  (25);


//looks at matchit results and makes table with sum of wine sales with matched target and control scv_ids
select tw_id,

sum(case when hybrid_scv_id in (select distinct scv_id from LAB_DS.J84536994.wines_promo_cust_wtr where target = 1) then sales end) as target,

sum(case when hybrid_scv_id in (select distinct scv_id from LAB_DS.J84536994.CONTROL_GROUP_full_x2 where control_flag = 'Control') then sales end) as control

from "CUR_LIVE_PCD"."PDL_TRANSFORM_STG_PROD"."W2_LINE_SCV_VX" a
left join CUR_LIVE_PCD.PDL_TRANSFORM_STG_PROD.D_RELATIVE_TRADING_DATE b using(tran_dt) 

where prod_id in (select prod_id
                  from "CUR_LIVE_PCD"."PDL_TRANSFORM_STG_PROD"."D_PROD_WTR" 
                where PROD_BUY_OFFICE_CD = 'WIN')
                 
and a.tran_dt between '2022-08-03' and '2023-01-24'

group by tw_id
ORDER BY tw_id;




and sales > 0 
group by scv_id;
