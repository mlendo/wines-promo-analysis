// similar to the base file but only for cellar

//collects scv_ids for customers that shopped cellar 
create or replace table LAB_DS.J84536994.pre_period_scv_id_cellar as
select distinct LATEST_CUST_SCV_ID as scv_id
from CUR_LIVE_PCD.PDL_TRANSFORM_STG_PROD.FBL_WDS_ORDER_SCV_VX
where order_dt between '2022-08-03' and '2022-11-01';


create or replace temporary table LAB_DS.J84536994.wines_cellar_tar_1 as
select scv_id, 
(total_cellar_spend) as total_cellar_spend,
(total_cellar_items) as total_cellar_items,
(total_cellar_visits) as total_cellar_visits,
'1' as target
from LAB_DS.J84536994.pre_period_scv_id_cellar A
left join LAB_DS.J84536994.cellar_tran C using (scv_id)
where scv_id in (select scv_id from LAB_DS.J84536994.wine_target_scv_id)



create or replace temporary table LAB_DS.J84536994.wines_cellar_cnt_2 as
select scv_id, 
(total_cellar_spend) as total_cellar_spend,
(total_cellar_items) as total_cellar_items,
(total_cellar_visits) as total_cellar_visits,
'0' as control
from LAB_DS.J84536994.pre_period_scv_id_cellar A
left join LAB_DS.J84536994.cellar_tran C using (scv_id)
where scv_id not in (select scv_id from LAB_DS.J84536994.wine_target_scv_id);


create or replace table LAB_DS.J84536994.wines_promo_cust_cellar as
select *
from LAB_DS.J84536994.wines_cellar_tar_1

union

select * 
from LAB_DS.J84536994.wines_cellar_cnt_2;



create or replace table LAB_DS.J84536994.wines_Cellar_daily_comp as

select order_dt,

sum(case when LATEST_CUST_SCV_ID in (select distinct scv_id from LAB_DS.J84536994.wines_promo_cust_cellar where target = 1) then ORDER_BILLING_AMT  end) as target,

sum(case when LATEST_CUST_SCV_ID in (select distinct scv_id from LAB_DS.J84536994.CONTROL_GROUP_full_x2 where control_flag = 'Control') then ORDER_BILLING_AMT end) as control

from CUR_LIVE_PCD.PDL_TRANSFORM_STG_PROD.FBL_WDS_ORDER_SCV_VX  a
left join CUR_LIVE_PCD.PDL_TRANSFORM_STG_PROD.D_RELATIVE_TRADING_DATE b on tran_dt = order_dt 

where wds_site_cd = 'CELLAR'
                 
and a.ORDER_DT between '2022-08-03' and '2023-01-24'

group by order_dt
ORDER BY order_dt;
