--code to collect partner scv ids
create or replace table LAB_DS.J84536994.partner_scv_ids as
--WAITROSE CUSTOMERS
select DISTINCT LATEST_CUST_SCV_ID 
FROM CUR_LIVE_PCD.PDL_TRANSFORM_STG_PROD.W1_TRAN_SCV_VX
WHERE disc_partner_amt <> 0
AND  tran_dt > dateadd(day,-91,getdate())
  UNION
--JL CUSTOMERS
select DISTINCT LATEST_CUST_SCV_ID
FROM  CUR_LIVE_PCD.PDL_TRANSFORM_STG_PROD.J1_TRAN_SCV_VX
WHERE disc_partner_amt <> 0
AND tran_dt > dateadd(day,-91,getdate())
//and USA_IND =1 ;

//value segement
create or replace table LAB_DS.J84536994.wine_cust_seg as 
select
HYBRID_SCV_ID as scv_id,
count(distinct tran_id) as visits,
sum(sales) as sales_a,
case when visits >= 12 and sales_a >= 1200 then 'BEST'
when visits >= 6 and sales_a >= 600 then 'GREAT'
when visits >= 2 and sales_a >= 300 then 'GOOD'
when visits >= 2 and sales_a < 300 then 'OCCASIONAL'
when visits = 1 then 'ONE TIMER' 
else 'NONE' end  as Segment
from CUR_LIVE_PCD.PDL_TRANSFORM_STG_PROD.W2_TRAN_SCV_VX
where tran_dt between '2022-08-03' and '2022-11-02'
and scv_id in (select scv_id from LAB_DS.J84536994.wine_target_scv_id)
and hybrid_pcc_type in ('C','S')
group by scv_id;

select 
scv_id
sum(sales) as spend
from FROM CUR_LIVE_PCD.PDL_TRANSFORM_STG_PROD.W2_LINE_SCV_VX
where prod_id in (select prod_id
                  from "CUR_LIVE_PCD"."PDL_TRANSFORM_STG_PROD"."D_PROD_WTR" 
                  where PROD_BUY_OFFICE_CD = 'WIN')


//target customer chareterstics table 
create or replace table LAB_DS.J84536994.wine_target_cust_chareteristics as
select 
a.scv_id,
a.channel,
AGE_5YR_BAND,
HHOLD_INCOME,
MYWTR_CARD_HOLDER_IND,
WTR_CATCHMENT_WDS_IND,
case when a.scv_id in (select latest_cust_scv_id from LAB_DS.J84536994.partner_scv_ids) then 1 else 0 end as partner_flag,
WTR_PREFERRED_BRANCH_NBR,
segment,
sales_a as wk13_spend,
combined_spend 
visits as wk13_visits

from LAB_DS.J84536994.wine_target_scv_id a
left join LAB_DS.VI4990.CACI_IND b on b.Latest_cust_scv_id = a.scv_id
left join CUR_LIVE_PCD.PDL_TRANSFORM_STG_PROD.CM_ALL_CUST_LIVE c on c.Latest_cust_scv_id = a.scv_id
left join LAB_DS.J84536994.wine_cust_seg d on a.scv_id = d.scv_id;

select 
channel,
avg(AGE_5YR_BAND),
avg(HHOLD_INCOME),
count(case when partner_flag=1 then scv_id end) as parnerts,
count(case when MYWTR_CARD_HOLDER_IND =1 then scv_id end) as my_wtr
from LAB_DS.J84536994.wine_target_cust_chareteristics
group by channel
